Source: kdbusaddons
Section: libs
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Maximiliano Curia <maxy@debian.org>
Build-Depends: cmake (>= 3.0~),
               dbus-x11,
               debhelper (>= 11~),
               doxygen,
               extra-cmake-modules (>= 5.51.0~),
               graphviz,
               libqt5sql5-sqlite:native,
               libqt5x11extras5-dev (>= 5.8.0~),
               pkg-kde-tools (>= 0.15.15ubuntu1~),
               qtbase5-dev (>= 5.8.0~),
               qttools5-dev (>= 5.4),
               qttools5-dev-tools (>= 5.4)
Standards-Version: 4.1.4
Homepage: https://projects.kde.org/projects/frameworks/kdbusaddons
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/kdbusaddons
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/kdbusaddons.git

Package: libkf5dbusaddons-bin
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: libkf5dbusaddons5-bin
Replaces: libkf5dbusaddons5-bin
Description: class library for qtdbus
 KDBusAddons provides convenience classes on top of QtDBus,
 as well as an API to create KDED modules.
 .
 This package contains kquitapp5.

Package: libkf5dbusaddons-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: libkf5dbusaddons5-data
Replaces: libkf5dbusaddons5-data
Description: class library for qtdbus
 KDBusAddons provides convenience classes on top of QtDBus,
 as well as an API to create KDED modules.
 .
 This package contains the translations.

Package: libkf5dbusaddons-dev
Section: libdevel
Architecture: any
Depends: libkf5dbusaddons5 (= ${binary:Version}),
         qtbase5-dev (>= 5.8.0~),
         ${misc:Depends}
Breaks: kded5-dev (<< 5.51),
        libkf5dbusaddons-doc (<< 5.61.90-0),
        libkf5service-dev (<< 5.51)
Replaces: libkf5dbusaddons-doc (<< 5.61.90-0)
Recommends: libkf5dbusaddons-doc (= ${source:Version})
Description: development files for dbusaddons
 KDBusAddons provides convenience classes on top of QtDBus,
 as well as an API to create KDED modules.

Package: libkf5dbusaddons-doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: class library for qtdbus (documentation)
 KDBusAddons provides convenience classes on top of QtDBus,
 as well as an API to create KDED modules.
 .
 This package contains the qch documentation files.
Section: doc

Package: libkf5dbusaddons5
Architecture: any
Multi-Arch: same
Depends: libkf5dbusaddons-data (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Breaks: baloo-kf5 (<< 5.51),
        kded5 (<< 5.51),
        kio (<< 5.51),
        libkf5globalaccel-bin (<< 5.51),
        libkf5kdelibs4support5-bin (<< 5.51),
        libkf5kiocore5 (<< 5.51),
        libkf5service5 (<< 5.51),
        libkf5wallet-bin (<< 5.51),
        plasma-framework (<< 5.25)
Recommends: libkf5dbusaddons-bin (= ${binary:Version})
Description: class library for qtdbus
 KDBusAddons provides convenience classes on top of QtDBus,
 as well as an API to create KDED modules.
